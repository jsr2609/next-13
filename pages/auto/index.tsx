import React from 'react'
import Button from "../../components/Button"
import Layout from "../../components/layout"
import type {NextPageWithLayout} from "../_app"

const Auto: NextPageWithLayout = () => {
  return (
    <div>
      <h1>LISTA DE AUTOS</h1>
      <Button/>
      
    </div>
  )
}

Auto.getLayout = function getLayout(page) {
  return(
    <Layout>
      {page}
    </Layout>
  )
}

export default Auto;