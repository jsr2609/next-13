import { ErrorMessage, Field, Form, Formik, FormikHelpers } from "formik";
import { GetServerSideProps, NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";
import type { Post } from '../../../types'

type Values = {
  title: string,
  body: string,
}
const Edit: NextPage<{post: Post}> = ({ post }) => {

  const router = useRouter();

  const handleSumbit = async (values: Values, { setSubmitting }: FormikHelpers<Values>) => {
    const options = {
      body: JSON.stringify(values),
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    }

    try {
      const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}`);
      const data = await response.json();
      
      router.push("/post");
      setSubmitting(false);
    } catch (error) {
      console.log("error");
    }
    

    
  }
  return (
    <div>
      Edit Post
      <div>
        <Formik
          onSubmit={handleSumbit}
          initialValues={{ title: post.title, body: post.body }}
        >
          {({ isSubmitting }) => (
            <Form>
              <div>
                <label>Title</label>
                <br />
                <Field type="text" name="title" />
                <ErrorMessage name="title" component="div" />
              </div>
              <div>
                <label>Body</label>
                <br />
                <Field as="textarea" cols={30} rows={5} name="body" />
                <ErrorMessage name="textarea" component="div" />
              </div>
              <div>
                <button type="submit" disabled={isSubmitting} >Actualizar</button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const id = context.params?.id;
  const response = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${id}`
  );
  const post = await response.json();

  return {
    props: {
      post,
    },
  };
};
export default Edit;
