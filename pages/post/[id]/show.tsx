import { GetServerSideProps, GetServerSidePropsContext, NextPage } from 'next'
import React from 'react'
import { useRouter } from "next/router";

import type { Post } from '../../../types'
import Link from 'next/link';

const Show: NextPage<{post: Post}> = ({post}) => {
  const router = useRouter();
  const {id} = router.query;
  console.log(id);
  return (
    <div>
        Show POST
        <div>
        <div><b>ID:</b> <br/>{post.id}</div>
        <div><b>TITLE:</b> <br/>{post.title}</div>
        <div><b>BODY:</b> <br/>{post.body}</div>
        </div>
        <div>
          <Link href={{pathname: '/post/[id]/edit', query: {id: post.id}}}>Editar</Link>
        </div>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const id = context.params?.id;
  const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
  const post = await response.json();

  return {
    props: {
      post
    }
  }
}

export default Show
