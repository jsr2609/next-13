import React, { ReactElement, useEffect, useState } from "react";
import axios from "axios";
import type { GetServerSideProps, NextPage } from "next";
import type { Post } from "../../types";
import Link from "next/link";

const Post: NextPage<{ posts: Post[] }> = ({ posts }) => {
  
  return (
    <div>
      LISTA POST
      <ul>
        <li><Link href={{pathname: '/post/new'}}>Nuevo</Link></li>
      </ul>
      
      <ul>
        {posts.map((post: Post) =>  {
          return (
            <li key={post.id}>
              <Link href={{
                pathname: "/post/[id]/show",
                query: {id: post.id}}}
              >{post.title}</Link> 
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const posts = await response.json();
  return {
    props: {
      posts,
    },
  };
};

export default Post;
