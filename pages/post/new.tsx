import React from "react";
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";
import * as Yup from 'yup';
import { useRouter } from "next/router";

interface Values { 
  title: string,
  body: string,
}

const validationSchema = Yup.object().shape({
  title: Yup.string()
    .min(10, "Demasiado Corto")
    .required('Este campo es obligatorio.'),
  body: Yup.string()
    .min(10, 'Demasiado corto')
    .required('Este campo es obligatorio.')
});

const New = () => {
  const router = useRouter();
  const handleSumbit = async (values: Values, { setSubmitting }: FormikHelpers<Values>) => {  

    const JSONData = JSON.stringify(values);
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSONData,
    };
    
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/posts",
      options
    );
  
    const result = await response.json();
    setSubmitting(false);
    router.push('/post')
  };
  return (
    <div>
      New Post
      <Formik
        initialValues={{ title: "", body: "" }}
        // validate={ values => {
        //   const errors = {};
        // }}
        onSubmit={handleSumbit} 
        validationSchema={validationSchema}
      >
        {({ isSubmitting }) => (
          <Form>
            <div>
              <label htmlFor="title">Título</label>
              <Field type="text" name="title" />
              <ErrorMessage name="title" component="div" />              
            </div>
            <div>
              <label htmlFor="cuerpo">Cuerpo</label>
              <Field as="textarea"  rows={5} cols={20} name="body" />
              <ErrorMessage name="body" component="div" />              
            </div>

            <button type="submit" disabled={isSubmitting}>Enviar</button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default New;
