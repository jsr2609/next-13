import React from 'react'
import styles from './Button.module.css'

const Button = () => {
  return (
    <button
        type="button" 
        className={styles.error}
    >
        Destroy
    </button>
  )
}

export default Button